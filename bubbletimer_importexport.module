<?php

define('BT_IE_NODE_TITLE', 0x00);
define('BT_IE_USERNAME',   0x01);
define('BT_IE_ACTIVITY',   0x02);
define('BT_IE_DATE',       0x03);

function bubbletimer_importexport_menu() {
	$items = array();

	$items['admin/settings/bubbletimer/import'] = array(
		'title' => 'Import',
		'type' => MENU_LOCAL_TASK,
		'page callback' => 'drupal_get_form',
		'page arguments' => array('bubbletimer_importform'),
		'access arguments' => array(BT_PERM_ADMIN),
	);

	$items['admin/settings/bubbletimer/export'] = array(
		'title' => 'Export',
		'type' => MENU_LOCAL_TASK,
		'page callback' => 'bubbletimer_export',
		'access arguments' => array(BT_PERM_ADMIN),
	);

	return $items;
}

function bubbletimer_importform() {
	$form = array();

	$form['csv'] = array(
		'#type' => 'textarea',
		'#title' => t('CSV'),
		'#cols' => 40,
		'#rows' => 10,
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Import',
	);

	return $form;
}

function bubbletimer_importform_submit($form, $form_state) {
	$arr = str_getcsv($form_state['values']['csv']);
	bubbletimer_import_check_for_duplicates($arr);
	bubbletimer_import($arr);
	drupal_set_message(t('Bubbletimer data imported.'));
}

function bubbletimer_import($arr) {
	foreach($arr as $record) {
		db_query('INSERT INTO {bubbletimer_activity}(nid, uid, activity, date) VALUES(
			(SELECT nid FROM {node} WHERE title = \'%s\' ORDER BY nid LIMIT 1),
			(SELECT uid FROM {users} WHERE name = \'%s\'),
			\'%s\',
			\'%s\'
		);', $record[BT_IE_NODE_TITLE], $record[BT_IE_USERNAME], $record[BT_IE_ACTIVITY], $record[BT_IE_DATE]);
	}
}

function bubbletimer_import_check_for_duplicates($arr) {
	$titles = array();
	foreach($arr as $a) $titles []= $a[BT_IE_NODE_TITLE];
	$titles = array_unique($titles);
	$titles = array_map('db_escape_string', $titles);
	$res = db_query('SELECT title, COUNT(nid) as c FROM {node} WHERE title IN (\''.join("','", $titles).'\') GROUP BY title HAVING c > 1;');
	$duplicates = array();
	while($row = db_fetch_array($res)) {
		$duplicates []= $row['title'];
	}
	array_unique($duplicates);
	foreach($duplicates as $d) {
		drupal_set_message(t('%node-title is a duplicate, using the lower node id.', array('%node-title', $d)), 'warning');
	}
}

function bubbletimer_export() {
	$arr = array();
	$res = db_query('SELECT n.title, u.name, b.activity, b.date
		FROM bubbletimer_activity b
		JOIN node n USING (nid)
		JOIN users u ON b.uid = u.uid
		ORDER BY b.date, b.nid;');
	while($row = db_fetch_array($res)) {
		$arr []= $row;
	}
	header('Content-Type: application/csv');
	header('Content-Disposition: attachment; filename=bubbletimer_'.time().'.csv');
	print str_putcsv($arr);
	return NULL;
}

if(!function_exists('str_getcsv')) {
	function str_getcsv($csv) {
		$ret = array();
		foreach(explode("\n", $csv) as $line) {
			$line = trim($line);
			if(!strlen($line) > 0) continue;
			$l = array();
			foreach(explode(',', $line) as $cell) {
				$l []= trim($cell, '"\'');
			}
			$ret []= $l;
		}
		return $ret;
	}
}

/**
 * Converts an array to csv.
 *
 * Copypasted from: Jeremy @ http://hu.php.net/manual/en/function.str-getcsv.php
 *
 * @param array $array
 * @param string $delimiter
 * @param string $enclosure
 * @param string $terminator
 * @return string
 */
function str_putcsv($array, $delimiter = ',', $enclosure = '"', $terminator = "\n") {
	# First convert associative array to numeric indexed array
	foreach ($array as $key => $value) $workArray[] = $value;

	$returnString = '';                 # Initialize return string
	$arraySize = count($workArray);     # Get size of array

	for ($i=0; $i<$arraySize; $i++) {
		# Nested array, process nest item
		if (is_array($workArray[$i])) {
			$returnString .= str_putcsv($workArray[$i], $delimiter, $enclosure, $terminator);
		} else {
			switch (gettype($workArray[$i])) {
			# Manually set some strings
				case "NULL":     $_spFormat = ''; break;
				case "boolean":  $_spFormat = ($workArray[$i] == true) ? 'true': 'false'; break;
				# Make sure sprintf has a good datatype to work with
				case "integer":  $_spFormat = '%i'; break;
				case "double":   $_spFormat = '%0.2f'; break;
				case "string":   $_spFormat = '%s'; break;
				# Unknown or invalid items for a csv - note: the datatype of array is already handled above, assuming the data is nested
				case "object":
				case "resource":
				default:         $_spFormat = ''; break;
			}
			$returnString .= sprintf('%2$s'.$_spFormat.'%2$s', $workArray[$i], $enclosure);
			$returnString .= ($i < ($arraySize-1)) ? $delimiter : $terminator;
		}
	}
	# Done the workload, return the output information
	return $returnString;
}